//Mini Activity:
/* 
    Setup a Basic Express JS SERVER


*/

const exp = require("express")
const mongoose = require("mongoose")
const taskRoute = require("../routes/taskRoute")

const app = exp();
const port = 4000;

app.use(exp.json());
app.use(exp.urlencoded({ extended: true }));


mongoose.connect('mongodb+srv://michaelchapoco:admin123@cluster0.dyhky.mongodb.net/toDo176?retryWrites=true&w=majority',
    {
        useNewUrlParser: true,
        useUnifiedTopology: true

    });

let db = mongoose.connection;

db.on('error', console.error.bind(console, "Connection Error"));
db.once('open', () => console.log('Connected to MongoDb'))


//Add the task route
//localhost:4000/tasks/
app.use("/tasks", taskRoute);

//Entry point response
app.listen(port, () => console.log('SERVER IS RUNNING AT PORT 4k!!!'))

